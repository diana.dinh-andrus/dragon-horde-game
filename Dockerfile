# select image
FROM maven:3.5-jdk-8

# ENVs
ENV DB_URL=jdbc:postgresql://dragonhordedb.cx1u2au4pz0d.us-east-2.rds.amazonaws.com/postgres
ENV DB_USER=postgres
ENV DB_PASS=password

# copy the project files
COPY ./pom.xml ./pom.xml

# build all dependencies for offline use
RUN mvn dependency:go-offline -B

# copy your other files
COPY ./src ./src

# build for release
RUN mvn package

# Expose port
EXPOSE 8080

# set the startup command to run your binary
CMD ["java", "-jar", "./target/DragonHorde-0.0.1-SNAPSHOT.jar"]


# ---------------------------------------------------
# our base build image
# FROM maven:3.5-jdk-8 as maven

# copy the project files
# COPY ./pom.xml ./pom.xml

# build all dependencies
# RUN mvn dependency:go-offline -B

# copy your other files
# COPY ./src ./src

# build for release
# RUN mvn package

# our final base image
# FROM openjdk:8u171-jre-alpine

# set deployment directory
# WORKDIR /DragonHorde

# copy over the built artifact from the maven image
# COPY --from=maven target/DragonHorde-*.jar ./

# set the startup command to run your binary
# CMD ["java", "-jar", "./target/DragonHorde.jar"]