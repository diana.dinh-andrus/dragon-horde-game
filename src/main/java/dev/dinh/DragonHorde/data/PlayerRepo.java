package dev.dinh.DragonHorde.data;

import dev.dinh.DragonHorde.models.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepo extends JpaRepository<Player, Integer> {
    public Player findPlayerById(int ID);
    //    List<Player> findAll();
}
