package dev.dinh.DragonHorde.data;

import dev.dinh.DragonHorde.models.Toon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface toonRepo extends JpaRepository<Toon, Integer> {
}
