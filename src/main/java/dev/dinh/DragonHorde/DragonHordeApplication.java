package dev.dinh.DragonHorde;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
public class DragonHordeApplication {

	public static void main(String[] args) {
		run(DragonHordeApplication.class, args);
	}

}
