package dev.dinh.DragonHorde.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Data
//@Getter @Setter @AllArgsConstructor @ToString @EqualsAndHashCode
@NoArgsConstructor @AllArgsConstructor

@Entity
@Table
@Component
public class Player {
    @Id
    @SequenceGenerator(
            name = "player_sequence",
            sequenceName = "StudentSequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "player_sequence"
    )
    private int id;
    private String username;
    private String authKey;
    private String email;

    @Autowired
    @OneToOne
    private Toon toon;

    public Player(String username, String password, String email, Toon toon) {
        this.username = username;
        this.authKey = password;
        this.email = email;
        this.toon = toon;
    }

    public Player(String username, String password, String email) {
        this.username = username;
        this.authKey = password;
        this.email = email;
    }

    public Player(int id, String username, String password, String email) {
        this.id = id;
        this.username = username;
        this.authKey = password;
        this.email = email;
    }
}
