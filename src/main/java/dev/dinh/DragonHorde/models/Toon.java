package dev.dinh.DragonHorde.models;

import org.springframework.stereotype.Component;
import lombok.Data;

import javax.persistence.*;

@Data

@Entity
@Table
@Component
public class Toon {
    @javax.persistence.Id
    @SequenceGenerator(
            name = "toon_sequence",
            sequenceName = "toonSequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "toon_sequence"
    )
    private int Id;

}
