package dev.dinh.DragonHorde.services;

import dev.dinh.DragonHorde.data.PlayerRepo;
import dev.dinh.DragonHorde.models.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class PlayerService {

    @Autowired
    private PlayerRepo playerRepo;

    public List<Player> getPlayers(){
        return playerRepo.findAll();
    }

    public Player createPlayer(Player player) {
        return playerRepo.save(player);
    }
}
