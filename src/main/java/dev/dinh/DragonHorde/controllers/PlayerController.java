package dev.dinh.DragonHorde.controllers;

import dev.dinh.DragonHorde.models.Player;
import dev.dinh.DragonHorde.models.PlayerToken;
import dev.dinh.DragonHorde.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "player")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @GetMapping
    public List<Player> getPlayers(){
        return playerService.getPlayers();
    }

    @PostMapping
    public ResponseEntity<String> registerPlayer(@RequestBody Player player){
        System.out.println(player);
        player = playerService.createPlayer(player);
        if(player == null){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        String token = PlayerToken.generateToken(player);
        return new ResponseEntity<>(token, HttpStatus.CREATED);
    }
}
